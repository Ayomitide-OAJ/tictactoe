module server

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dawidd6/go-mmap v0.0.0-20190117214105-bbc5b2bc8878
	github.com/golang/protobuf v1.4.2
	github.com/husobee/vestigo v1.1.1
	github.com/stretchr/testify v1.3.0 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4 // indirect
)
