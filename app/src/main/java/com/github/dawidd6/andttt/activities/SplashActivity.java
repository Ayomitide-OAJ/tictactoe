package com.github.dawidd6.andttt.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.github.dawidd6.andttt.R;

public class SplashActivity extends Activity {

    private static int SPLASH_TIME_OUT = 5000;
    private Button playButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        playButton = (Button) findViewById(R.id.playButton);

        YoYo.with(Techniques.SlideInUp)
                .duration(5000)
                .playOn(findViewById(R.id.playButton));

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        });
    }
}
